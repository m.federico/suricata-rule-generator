#!/usr/bin/python3

import argparse
import os

# proto, source, source port, direzione, dest, dest port, messaggio, argomenti, sid, rev
template = "alert {} {} {} {} {} {} (msg:\"{}\"; {} sid:{}; rev:{};)"


def parse():
    # prepara il parser
    parser = argparse.ArgumentParser(description='Suricata rule generator')
    parser.add_argument("proto", help="protocol", action="store")
    parser.add_argument("--src", help="source address", action="store", dest="src")
    parser.add_argument("--dst", help="destination address", action="store", dest="dst")
    parser.add_argument("--sport", help="source port", action="store", dest="sport")
    parser.add_argument("--dport", help="destination port", action="store", dest="dport")
    parser.add_argument("--bid", help="bidirectional", action="store_true", dest="bid")
    parser.add_argument("msg", help="log message", action="store")
    parser.add_argument("sid", help="signature ID", action="store")
    parser.add_argument("--rev", help="revision number", action="store", dest="rev")
    parser.add_argument("--opt", help="suricata rule's option. If multiple --opt are used they should appear in the order they are desired in the output rule", action="append", dest="opt")
    parser.add_argument("--list", help="list of contents to filter", action="store", dest="cont")

    return parser.parse_args()


def main():
    src = "any"
    dst = "any"
    sport = "any"
    dport = "any"
    bid = "->"
    rev = "1"
    args = []

    matches = parse()

    if matches.src:
        src = matches.src
    if matches.dst:
        dst = matches.dst
    if matches.sport:
        sport = matches.sport
    if matches.dport:
        dport = matches.dport
    if matches.bid:
        bid = "<>"
    if matches.rev:
        rev = matches.rev
    if matches.opt:
        args = matches.opt

    args = [x + "; " for x in args]
    args = "".join(args).strip()

    if matches.cont:
        if os.path.isfile(matches.cont):
            with open(matches.cont) as f:
                for c in f: 
                    new_args = "content: \"{}\"; {}".format(c.strip(), args)
                    print(template.format(matches.proto, src, sport, bid, dst, dport,
                                          matches.msg, new_args, matches.sid, rev))
                    new_sid = int(matches.sid) +1
                    matches.sid = new_sid
        else:
            print("File {} doesn't exists".format(matches.cont))

    else:
        print(template.format(matches.proto, src, sport, bid, dst, dport, matches.msg, args, matches.sid, rev))


if __name__ == '__main__':
    main()
